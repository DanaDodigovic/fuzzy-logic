#include "Defuzzifier.hpp"
#include "Assert.hpp"

int COADefuzzifier::defuzzify(IFuzzySetP fuzzy_set) noexcept
{
    ASSERT(fuzzy_set->get_domain().get_n_components() == 1,
           "Fuzzy set should have simple domain.\n");

    double numerator   = 0;
    double denominator = 0;

    for (const auto& element : fuzzy_set->get_domain()) {
        numerator += element[0] * fuzzy_set->get_value_at(element);
        denominator += fuzzy_set->get_value_at(element);
    }

    if (denominator == 0) {
        // Return first element from domain.
        return fuzzy_set->get_domain().element_for_index(0)[0];
    }

    int result = std::round(numerator / denominator);

    return result;
}
