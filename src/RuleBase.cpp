#include "RuleBase.hpp"

void RuleBase::add_rule(RuleP rule) noexcept
{
    rules.push_back(std::move(rule));
}

std::size_t RuleBase::get_n_rules() const noexcept
{
    return rules.size();
}

std::vector<RuleP>::const_iterator RuleBase::begin() const noexcept
{
    return rules.begin();
}

std::vector<RuleP>::const_iterator RuleBase::end() const noexcept
{
    return rules.end();
}
