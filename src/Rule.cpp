#include "Rule.hpp"
#include "Assert.hpp"

Rule::Rule(IBinaryFunctionSP t_norm, IBinaryFunctionSP implication)
    : t_norm(std::move(t_norm)), implication(std::move(implication))
{}

IFuzzySetP Rule::conclude(SystemInput input) const
{
    ASSERT(antecedents.size() > 0, "Antecedent vector should not be empty.\n");
    ASSERT(consequent != nullptr, "Consequent is not set.\n");

    auto conclusion =
        std::make_unique<MutableFuzzySet>(get_consequent_domain());

    auto in = get_rule_inputs(input);

    // conjunction = mu(input))
    double conj = antecedents[0]->get_value_at(DomainElement::of({in[0]}));
    for (std::size_t i = 1; i < in.size(); ++i) {
        // conjunction = t_norm(mu(conjunction), mu(input))
        conj = t_norm->value_at(
            conj, antecedents[i]->get_value_at(DomainElement::of({in[i]})));
    }

    // mu(conclusion_elem) = implication(mu(conjunction), mu(conseq_elem))
    for (const auto& element : consequent->get_domain()) {
        double cons_value = consequent->get_value_at(element);
        double impl_value = implication->value_at(conj, cons_value);
        conclusion->set(element, impl_value);
    }

    return conclusion;
}

IDomainP Rule::get_consequent_domain() const noexcept
{
    const auto& d = consequent->get_domain();
    return Domain::int_range((*d.get_component(0).begin())[0],
                             (*d.get_component(0).begin())[0] +
                                 d.get_cardinality());
}
