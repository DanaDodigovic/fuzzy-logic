#include "RudderRules.hpp"
#include "Config.hpp"

// If L is critically small, make medium change of rudder angle.
LRudderRule::LRudderRule(IBinaryFunctionSP t_norm,
                         IBinaryFunctionSP implication)
    : Rule(std::move(t_norm), std::move(implication))
{
    std::vector<IFuzzySetP> vec;
    antecedents.push_back(std::make_unique<CalculatedFuzzySet>(
        Domain::int_range(start_dist, end_dist),
        StandardFuzzySets::l_function(15, 25)));

    consequent = std::make_unique<CalculatedFuzzySet>(
        Domain::int_range(start_ang, end_ang),
        StandardFuzzySets::lambda_function(70, 80, 90));
}

std::vector<int> LRudderRule::get_rule_inputs(SystemInput input) const noexcept
{
    return {input.L};
}

// If R is critically small, make medium change of rudder angle.
RRudderRule::RRudderRule(IBinaryFunctionSP t_norm,
                         IBinaryFunctionSP implication)
    : Rule(std::move(t_norm), std::move(implication))
{
    std::vector<IFuzzySetP> vec;
    antecedents.push_back(std::make_unique<CalculatedFuzzySet>(
        Domain::int_range(start_dist, end_dist),
        StandardFuzzySets::l_function(15, 25)));

    consequent = std::make_unique<CalculatedFuzzySet>(
        Domain::int_range(start_ang, end_ang),
        StandardFuzzySets::lambda_function(90, 100, 110));
}

std::vector<int> RRudderRule::get_rule_inputs(SystemInput input) const noexcept
{
    return {input.R};
}

// If LK is critically small, make medium change of rudder angle.
LKRudderRule::LKRudderRule(IBinaryFunctionSP t_norm,
                           IBinaryFunctionSP implication)
    : Rule(std::move(t_norm), std::move(implication))
{
    std::vector<IFuzzySetP> vec;
    antecedents.push_back(std::make_unique<CalculatedFuzzySet>(
        Domain::int_range(start_dist, end_dist),
        StandardFuzzySets::l_function(40, 50)));

    consequent = std::make_unique<CalculatedFuzzySet>(
        Domain::int_range(start_ang, end_ang),
        StandardFuzzySets::lambda_function(0, 30, 90));
}

std::vector<int> LKRudderRule::get_rule_inputs(SystemInput input) const noexcept
{
    return {input.LK};
}

// If RK is critically small, make medium change of rudder angle.
RKRudderRule::RKRudderRule(IBinaryFunctionSP t_norm,
                           IBinaryFunctionSP implication)
    : Rule(std::move(t_norm), std::move(implication))
{
    std::vector<IFuzzySetP> vec;
    antecedents.push_back(std::make_unique<CalculatedFuzzySet>(
        Domain::int_range(start_dist, end_dist),
        StandardFuzzySets::l_function(40, 50)));

    consequent = std::make_unique<CalculatedFuzzySet>(
        Domain::int_range(start_ang, end_ang),
        StandardFuzzySets::lambda_function(90, 150, 180));
}

std::vector<int> RKRudderRule::get_rule_inputs(SystemInput input) const noexcept
{
    return {input.RK};
}
