#include "AccelerationRules.hpp"
#include "FuzzySystem.hpp"
#include "Operation.hpp"
#include "SystemInput.hpp"

#include <iostream>
#include <sstream>

#include <fmt/core.h>

int main()
{
    AccelerationFuzzySystem acc_fuzzy_system(std::make_unique<COADefuzzifier>(),
                                             std::make_shared<ZadehAnd>(),
                                             std::make_shared<ZadehAnd>());
    RudderFuzzySystem rudder_fuzzy_system(std::make_unique<COADefuzzifier>(),
                                          std::make_shared<ZadehAnd>(),
                                          std::make_shared<ZadehAnd>());

    while (true) {
        std::string lnIn;
        std::getline(std::cin, lnIn);
        if (lnIn == "KRAJ") { break; }
        SystemInput input(lnIn);

        std::cout << acc_fuzzy_system.conclude(input) << " "
                  << rudder_fuzzy_system.conclude(input) << std::endl;
    }
}
