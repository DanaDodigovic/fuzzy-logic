#include "AccelerationRules.hpp"
#include "Config.hpp"

// If L is critically small, accelerate with medium acceleration.
LAccelerationRule::LAccelerationRule(IBinaryFunctionSP t_norm,
                                     IBinaryFunctionSP implication)
    : Rule(std::move(t_norm), std::move(implication))
{
    std::vector<IFuzzySetP> vec;
    antecedents.push_back(std::make_unique<CalculatedFuzzySet>(
        Domain::int_range(start_dist, end_dist),
        StandardFuzzySets::l_function(20, 30)));

    consequent = std::make_unique<CalculatedFuzzySet>(
        Domain::int_range(start_acc, end_acc),
        StandardFuzzySets::lambda_function(13, 15, 20));
}

std::vector<int>
LAccelerationRule::get_rule_inputs(SystemInput input) const noexcept
{
    return {input.L};
}

// If R is critically small, accelerate with medium acceleration.
RAccelerationRule::RAccelerationRule(IBinaryFunctionSP t_norm,
                                     IBinaryFunctionSP implication)
    : Rule(std::move(t_norm), std::move(implication))
{
    std::vector<IFuzzySetP> vec;
    antecedents.push_back(std::make_unique<CalculatedFuzzySet>(
        Domain::int_range(start_dist, end_dist),
        StandardFuzzySets::l_function(20, 30)));

    consequent = std::make_unique<CalculatedFuzzySet>(
        Domain::int_range(start_acc, end_acc),
        StandardFuzzySets::lambda_function(13, 15, 20));
}

std::vector<int>
RAccelerationRule::get_rule_inputs(SystemInput input) const noexcept
{
    return {input.R};
}

// If LK is critically small, accelerate with medium acceleration.
LKAccelerationRule::LKAccelerationRule(IBinaryFunctionSP t_norm,
                                       IBinaryFunctionSP implication)
    : Rule(std::move(t_norm), std::move(implication))
{
    std::vector<IFuzzySetP> vec;
    antecedents.push_back(std::make_unique<CalculatedFuzzySet>(
        Domain::int_range(start_dist, end_dist),
        StandardFuzzySets::l_function(40, 50)));

    consequent = std::make_unique<CalculatedFuzzySet>(
        Domain::int_range(start_acc, end_acc),
        StandardFuzzySets::lambda_function(13, 15, 20));
}

std::vector<int>
LKAccelerationRule::get_rule_inputs(SystemInput input) const noexcept
{
    return {input.LK};
}

// If RK is critically small, accelerate with medium acceleration.
RKAccelerationRule::RKAccelerationRule(IBinaryFunctionSP t_norm,
                                       IBinaryFunctionSP implication)
    : Rule(std::move(t_norm), std::move(implication))
{
    std::vector<IFuzzySetP> vec;
    antecedents.push_back(std::make_unique<CalculatedFuzzySet>(
        Domain::int_range(start_dist, end_dist),
        StandardFuzzySets::l_function(40, 50)));

    consequent = std::make_unique<CalculatedFuzzySet>(
        Domain::int_range(start_acc, end_acc),
        StandardFuzzySets::lambda_function(13, 15, 20));
}

std::vector<int>
RKAccelerationRule::get_rule_inputs(SystemInput input) const noexcept
{
    return {input.RK};
}

// If direction is wrong, accelerate with medium acceleration.
DirectionRule::DirectionRule(IBinaryFunctionSP t_norm,
                             IBinaryFunctionSP implication)
    : Rule(std::move(t_norm), std::move(implication))
{
    std::vector<IFuzzySetP> vec;
    antecedents.push_back(std::make_unique<CalculatedFuzzySet>(
        Domain::int_range(0, 2), StandardFuzzySets::l_function(0, 1)));

    consequent = std::make_unique<CalculatedFuzzySet>(
        Domain::int_range(start_acc, end_acc),
        StandardFuzzySets::lambda_function(14, 15, 16));
}

std::vector<int>
DirectionRule::get_rule_inputs(SystemInput input) const noexcept
{
    return {input.D};
}

// If velocity is big, decrease acceleration.
VelocityRule::VelocityRule(IBinaryFunctionSP t_norm,
                           IBinaryFunctionSP implication)
    : Rule(std::move(t_norm), std::move(implication))
{
    std::vector<IFuzzySetP> vec;
    antecedents.push_back(std::make_unique<CalculatedFuzzySet>(
        Domain::int_range(start_vel, end_velocity),
        StandardFuzzySets::gamma_function(20, 40)));

    consequent = std::make_unique<CalculatedFuzzySet>(
        Domain::int_range(start_acc, end_acc),
        StandardFuzzySets::lambda_function(4, 7, 10));
}

std::vector<int> VelocityRule::get_rule_inputs(SystemInput input) const noexcept
{
    return {input.V};
}

// If L, LK, D and DK are relatively big, accelerate with medium acceleration.
ClearAccelerationRule::ClearAccelerationRule(IBinaryFunctionSP t_norm,
                                             IBinaryFunctionSP implication)
    : Rule(std::move(t_norm), std::move(implication))
{
    std::vector<IFuzzySetP> vec;
    antecedents.push_back(std::make_unique<CalculatedFuzzySet>(
        Domain::int_range(start_dist, end_dist),
        StandardFuzzySets::l_function(230, 300)));

    antecedents.push_back(std::make_unique<CalculatedFuzzySet>(
        Domain::int_range(start_dist, end_dist),
        StandardFuzzySets::l_function(230, 300)));

    antecedents.push_back(std::make_unique<CalculatedFuzzySet>(
        Domain::int_range(start_dist, end_dist),
        StandardFuzzySets::l_function(230, 300)));

    antecedents.push_back(std::make_unique<CalculatedFuzzySet>(
        Domain::int_range(start_dist, end_dist),
        StandardFuzzySets::l_function(230, 300)));

    consequent = std::make_unique<CalculatedFuzzySet>(
        Domain::int_range(start_acc, end_acc),
        StandardFuzzySets::lambda_function(10, 11, 12));
}

std::vector<int>
ClearAccelerationRule::get_rule_inputs(SystemInput input) const noexcept
{
    return {input.L, input.LK, input.R, input.RK};
}
