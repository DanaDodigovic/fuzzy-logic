#include "SystemInput.hpp"

#include <sstream>

SystemInput::SystemInput(std::string input)
{
    std::istringstream(input) >> L >> R >> LK >> RK >> V >> D;
}
