#include <catch2/catch.hpp>

#include "Relation.hpp"
#include "AccelerationRules.hpp"
#include "Operation.hpp"
#include "FuzzySystem.hpp"

#include <iostream>
#include <fmt/core.h>

TEST_CASE("Test1")
{
    AccelerationFuzzySystem acc_fuzzy_system(std::make_unique<COADefuzzifier>(),
                                             std::make_shared<ZadehAnd>(),
                                             std::make_shared<ZadehAnd>());

    acc_fuzzy_system.print_conclusion(
        std::make_unique<VelocityRule>(Operation::zadeh_and(),
                                       Operation::zadeh_and()),
        SystemInput("10 260 42 240 30 0"));

    acc_fuzzy_system.print_conclusion(SystemInput("10 260 42 240 30 0"));
}
