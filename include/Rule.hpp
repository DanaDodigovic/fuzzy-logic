#ifndef RULE_HPP
#define RULE_HPP

#include "FuzzySet.hpp"
#include "OperationFunction.hpp"
#include "SystemInput.hpp"

#include <vector>

class Rule {
  protected:
    std::vector<IFuzzySetP> antecedents;
    IFuzzySetP consequent;
    IBinaryFunctionSP t_norm;
    IBinaryFunctionSP implication;

  public:
    Rule(IBinaryFunctionSP t_norm, IBinaryFunctionSP implication);
    virtual ~Rule() = default;

    IFuzzySetP conclude(SystemInput input) const;

  protected:
    virtual std::vector<int>
    get_rule_inputs(SystemInput input) const noexcept = 0;
    IDomainP get_consequent_domain() const noexcept;

  private:
    double get_min(const DomainElement& element) noexcept;
};

using RuleP = std::unique_ptr<Rule>;

#endif
