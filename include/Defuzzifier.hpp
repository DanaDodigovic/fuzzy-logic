#ifndef DEFUZZIFIER_HPP
#define DEFUZZIFIER_HPP

#include "FuzzySet.hpp"

#include <memory>

class Defuzzifier {
  public:
    virtual ~Defuzzifier() = default;

    virtual int defuzzify(IFuzzySetP fuzzy_set) = 0;
};

using DefuzzifierP = std::unique_ptr<Defuzzifier>;

class COADefuzzifier : public Defuzzifier {
  public:
    int defuzzify(IFuzzySetP fuzzy_set) noexcept override;
};

#endif
