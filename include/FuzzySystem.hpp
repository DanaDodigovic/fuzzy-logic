#ifndef FUZZY_SYSTEM_HPP
#define FUZZY_SYSTEM_HPP

#include "RuleBase.hpp"
#include "Operation.hpp"
#include "Defuzzifier.hpp"
#include "SystemInput.hpp"

class FuzzySystem {
  protected:
    RuleBase rule_base;
    DefuzzifierP def;
    IFuzzySetP consequent = nullptr;

  public:
    FuzzySystem(DefuzzifierP def) noexcept;
    FuzzySystem(DefuzzifierP def, RuleBase) noexcept;

    int conclude(SystemInput input);

    void print_conclusion(RuleP rule, SystemInput input);
    void print_conclusion(SystemInput input);

  private:
    std::vector<IFuzzySetP> get_conslusions(SystemInput input) const noexcept;
    IFuzzySetP get_union(std::vector<IFuzzySetP> sets) const;
    IDomainP get_consequent_domain() const;
};

class AccelerationFuzzySystem : public FuzzySystem {
  public:
    AccelerationFuzzySystem(DefuzzifierP def,
                            IBinaryFunctionSP t_norm,
                            IBinaryFunctionSP implication);
};

class RudderFuzzySystem : public FuzzySystem {
  public:
    RudderFuzzySystem(DefuzzifierP def,
                      IBinaryFunctionSP t_norm,
                      IBinaryFunctionSP implication);
};

#endif
