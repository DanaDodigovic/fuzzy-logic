#ifndef RUDDER_RULES_HPP
#define RUDDER_RULES_HPP

#include "Rule.hpp"

class LRudderRule : public Rule {
  private:
    std::vector<int> get_rule_inputs(SystemInput input) const noexcept override;

  public:
    LRudderRule(IBinaryFunctionSP t_norm, IBinaryFunctionSP implication);
};

class RRudderRule : public Rule {
  private:
    std::vector<int> get_rule_inputs(SystemInput input) const noexcept override;

  public:
    RRudderRule(IBinaryFunctionSP t_norm, IBinaryFunctionSP implication);
};

class LKRudderRule : public Rule {
  private:
    std::vector<int> get_rule_inputs(SystemInput input) const noexcept override;

  public:
    LKRudderRule(IBinaryFunctionSP t_norm, IBinaryFunctionSP implication);
};

class RKRudderRule : public Rule {
  private:
    std::vector<int> get_rule_inputs(SystemInput input) const noexcept override;

  public:
    RKRudderRule(IBinaryFunctionSP t_norm, IBinaryFunctionSP implication);
};

#endif
