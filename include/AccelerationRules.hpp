#ifndef ACCELERATION_RULES_HPP
#define ACCELERATION_RULES_HPP

#include "Rule.hpp"

class ClearAccelerationRule : public Rule {
  private:
    std::vector<int> get_rule_inputs(SystemInput input) const noexcept override;

  public:
    ClearAccelerationRule(IBinaryFunctionSP t_norm,
                          IBinaryFunctionSP implication);
};

class LAccelerationRule : public Rule {
  private:
    std::vector<int> get_rule_inputs(SystemInput input) const noexcept override;

  public:
    LAccelerationRule(IBinaryFunctionSP t_norm, IBinaryFunctionSP implication);
};

class RAccelerationRule : public Rule {
  private:
    std::vector<int> get_rule_inputs(SystemInput input) const noexcept override;

  public:
    RAccelerationRule(IBinaryFunctionSP t_norm, IBinaryFunctionSP implication);
};

class LKAccelerationRule : public Rule {
  private:
    std::vector<int> get_rule_inputs(SystemInput input) const noexcept override;

  public:
    LKAccelerationRule(IBinaryFunctionSP t_norm, IBinaryFunctionSP implication);
};

class RKAccelerationRule : public Rule {
  private:
    std::vector<int> get_rule_inputs(SystemInput input) const noexcept override;

  public:
    RKAccelerationRule(IBinaryFunctionSP t_norm, IBinaryFunctionSP implication);
};

class DirectionRule : public Rule {
  private:
    std::vector<int> get_rule_inputs(SystemInput input) const noexcept override;

  public:
    DirectionRule(IBinaryFunctionSP t_norm, IBinaryFunctionSP implication);
};

class VelocityRule : public Rule {
  private:
    std::vector<int> get_rule_inputs(SystemInput input) const noexcept override;

  public:
    VelocityRule(IBinaryFunctionSP t_norm, IBinaryFunctionSP implication);
};

#endif
