#ifndef RULE_BASE_HPP
#define RULE_BASE_HPP

#include "Rule.hpp"

class RuleBase {
  private:
    std::vector<RuleP> rules;

  public:
    void add_rule(RuleP rule) noexcept;
    std::size_t get_n_rules() const noexcept;

    std::vector<RuleP>::const_iterator begin() const noexcept;
    std::vector<RuleP>::const_iterator end() const noexcept;
};

#endif
